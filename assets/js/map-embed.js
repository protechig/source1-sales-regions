'use strict';

// Do not draw Alaska or Hawaii.
var EXCLUDED_STATES = d3.set([2, 15]);

var width = 800,
    height = 400;

var pScale = 900;

var projection = d3.geo.albersUsa()
    .scale(pScale)
    .translate([width / 2, height / 2]);

var path = d3.geo.path()
    .projection(projection);

var svg = d3.select('#map').append('svg')
    .attr('preserveAspectRatio', 'xMinYMin meet')
    .attr('viewBox', '0 0 800 400');

function selectAgent(name) {
  jQuery('#sr-reps > #please-select').remove();
  jQuery('#sr-reps .rep.selected').removeClass('selected');
  jQuery('#sr-reps .rep#' + name).addClass('selected');
}

d3.json($STATES_JSON, function (err, us) {
  if (err) {
    throw err;
  }

  var visibleStates = topojson
      .feature(us, us.objects.states)
      .features
      .filter(function (f) {
        return !EXCLUDED_STATES.has(f.id);
      });

  svg.append('g')
    .attr('class', 'states')
    .selectAll('path')
    .data(visibleStates)
    .enter()
    .append('path')
    .attr('class', 'state')
    .attr('d', path);

  svg.append('path')
    .datum(topojson.mesh(us, us.objects.states, function (a, b) {
      return a !== b;
    }))
    .attr('class', 'state-boundary')
    .attr('d', path);

  d3.json($REGIONS_JSON, function (err, rs) {
    rs.regions.forEach(function (region) {
      var inRegion = d3.set(region.states);
      var regionGeos = us.objects.states.geometries.filter(function (d) {
        return inRegion.has(d.id);
      });

      svg.append('path')
        .datum(topojson.merge(us, regionGeos))
        .style({
          fill: region.color,
          stroke: '#e1e1e1',
          'stroke-width': 2
        })
        .attr('class', 'state selected')
        .attr('d', path)
        .on('mouseover', function (d) {
          d3.select(this).style('fill', d3.rgb(region.color).brighter(0.7));
        })
        .on('mouseout', function (d) {
          d3.select(this).style('fill', region.color);
        })
        .on('click', function (d) {
          selectAgent(region.repAnchor);
        });
    });
  });
});

d3.select(self.frameElement).style('height', height + 'px');
