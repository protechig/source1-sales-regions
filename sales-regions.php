<?php
/**
 * Plugin Name: Source1 Sales Region Map
 * Plugin URI: http://regions.wordpress.dev (FIXME)
 * Description: Provides a shortcode to render a map of US sales regions.
 * Version: 0.0.1
 * Author: ProTech Internet Group
 */

if (!defined('WPINC')) {
  die();
}

define('S1SR_VERSION', '0.0.1');
define('S1SR_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('S1SR_PLUGIN_URL', plugins_url('', __FILE__));

define('S1SR_SHORTCODE', 's1sr-map');

/**
 * Required JavaScript includes (d3, topojson, and the map embed).
 */
const S1SR_SCRIPTS = array(
  array(
    'handle'    => 's1sr-d3',
    'src'       => '//d3js.org/d3.v3.min.js',
    'deps'      => array(),
    'ver'       => false,
    'in_footer' => true
  ),
  array(
    'handle'    => 's1sr-topojson',
    'src'       => '//d3js.org/topojson.v1.min.js',
    'deps'      => array(),
    'ver'       => false,
    'in_footer' => true
  ),
  array(
    'handle'    => 's1sr-map',
    'src'       => S1SR_PLUGIN_URL . '/assets/js/map-embed.js',
    'deps'      => array('s1sr-d3', 's1sr-topojson'),
    'ver'       => null,
    'in_footer' => true
  )
);

/**
 * Register required scripts.
 */
function s1sr_register_scripts() {
  foreach (S1SR_SCRIPTS as $s) {
    wp_register_script(
      $s['handle'],
      $s['src'],
      $s['deps'],
      $s['ver'],
      $s['in_footer']
    );
  }
}
add_action('wp_enqueue_scripts', 's1sr_register_scripts');

/**
 * Evaluate the s1sr-map shortcode.
 *
 * This enqueues scripts registered above, defines locations
 * to requisite JSON files, and outputs the map HTML.
 *
 * @param array $attrs the shortcode attributes
 */
function s1sr_map_embed($attrs) {
  $asset_dir = S1SR_PLUGIN_URL . '/assets';

  echo file_get_contents(S1SR_PLUGIN_DIR . '/assets/shortcode-embed.html');

  ob_start();
  ?>
  <script>
   var $STATES_JSON = '<?php echo S1SR_PLUGIN_URL . '/assets/us.json'; ?>';
   var $REGIONS_JSON = '<?php echo S1SR_PLUGIN_URL . '/assets/regions.json'; ?>';
  </script>
  <?php
  echo ob_get_clean();

  foreach (S1SR_SCRIPTS as $s) {
    wp_enqueue_script($s['handle']);
  }
}
add_shortcode(S1SR_SHORTCODE, s1sr_map_embed);
